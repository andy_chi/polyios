//
//  Backend.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import Alamofire


class Backend {
    
    static let rootURL = "http://nulp-maxapi.rhcloud.com"
    
    class func getInstitutes(callback: (NSError?, NSURLRequest?, NSHTTPURLResponse?, [Institute]) -> Void ) {
        Alamofire.request(.GET, rootURL + "/institutes").response { (request, response, data, error) in
            guard error == nil && data != nil else {
                return callback(error, request, response, [])
            }
            
            if let response_ = response where response_.statusCode > 400 {
                return callback(error, request, response, [])
            }
        
            InstitutesParser().parse(data!, callback: { (error, institutes) in
                callback(error, request, response, institutes ?? [])
            })
            
        }
    }
    
    class func getSchedule(group: Group,
                           callback: (NSError?, NSURLRequest?, NSHTTPURLResponse?, Schedule?) -> Void,
                           progress: ((bytesRead:Int64, totalBytesRead:Int64, totalBytesExpectedToRead:Int64) -> Void)? = nil) {
        
        guard let instituteCode = group.institute?.index else {
           return callback(NSError(domain: "group is not persisted", code: 100, userInfo: nil), nil, nil, nil)
        }
        
        Alamofire.request(.GET, rootURL + "/schedule/\(instituteCode)/\(group.index)").progress(progress).response { (request, response, data, error) in
            
            guard error == nil && data != nil else {
                return callback(error, request, response, nil)
            }
            
            if let response_ = response where response_.statusCode > 400 {
                return callback(error, request, response, nil)
            }
            
            
            ScheduleParser().parse(data!, callback: { (error, schedule) in
                dispatch_async(dispatch_get_main_queue()) {
                schedule?.group = group
                callback(error, request, response, schedule)
                }
                
            })
            
        }
    }
    
    class func getStatus(callback: (NSError?, NSURLRequest?, NSHTTPURLResponse?, Schedule?) -> Void) {
        
        Alamofire.request(.GET, rootURL + "/status").response { (request, response, data, error) in
            guard error == nil && data != nil else {
                return callback(error, request, response, nil)
            }
            
            if let response_ = response where response_.statusCode > 400 {
                return callback(error, request, response, nil)
            }
            
            //TODO: Parse status
        }
    }
}





