//
//  Schedule.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

class Schedule: Object {
    dynamic var id: Int = 0 // group.index
    
    var group: Group? {
        didSet {
            if let group = group {
                id = group.index
            }
        }
    }
    
    dynamic var autumn_part1: WeekSchedule?
    dynamic var autumn_part2: WeekSchedule?
    dynamic var spring_part1: WeekSchedule?
    dynamic var spring_part2: WeekSchedule?
    
    override class func primaryKey() -> String? {
        return "id"
    }
}