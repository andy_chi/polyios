//
//  AppDelegate.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/11/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        window?.rootViewController = MainViewController()
        
        window?.makeKeyAndVisible()
        
        configureNavBarAppearance()
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        configureTabBarAppearance()
        return true
    }
    
    func configureTabBarAppearance() {
        UITabBar.appearance().barStyle = UIBarStyle.Default
        UITabBar.appearance().translucent = false
        UITabBar.appearance().shadowImage = nil
        UITabBar.appearance().tintColor = .whiteColor()
        UITabBar.appearance().barTintColor = UIColor(red:0.24, green:0.36, blue:0.47, alpha:1.00)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        
    }
    
    func configureNavBarAppearance() {
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().barTintColor = UIColor(red:0.24, green:0.36, blue:0.47, alpha:1.00)
        UINavigationBar.appearance().tintColor = UIColor(red:0.62, green:0.70, blue:0.78, alpha:1.00)
    }

    func applicationWillResignActive(application: UIApplication) {
        //dump(self.window?.rootViewController?.childViewControllers.first!)
        print(self.window?.rootViewController?.childViewControllers.first!.presentedViewController)
       // dump(self.window?.rootViewController?.childViewControllers.first!.presentingViewController)
       // self.window?.rootViewController?.childViewControllers.first!.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
     //   self.window?.rootViewController?.childViewControllers.first!.dismissViewControllerAnimated(false, completion: nil)
        
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

