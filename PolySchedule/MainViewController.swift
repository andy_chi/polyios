//
//  MainViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    var startupViewController: StartUpViewController?
    var tabBarViewController: UITabBarController?
    
    var currentViewController: UIViewController?
    
    weak var observer: AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        // Do any additional setup after loading the view.
        
        
        observer = NSNotificationCenter.defaultCenter().addObserverForName(STARTUP_COMPLETED_NOTIFICATION, object: nil, queue: .mainQueue()) { (note) in
            
            //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC)), dispatch_get_main_queue()) {
                self.showTabBarViewController()
            //}
            
            
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if !userDefaults.boolForKey("startup_completed") {
            showStartupViewController()
        } else {
            showTabBarViewController()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


    
    func showStartupViewController() {
        if let currentViewController = currentViewController,
        let startupViewController = startupViewController where currentViewController == startupViewController{
            return
        }
        
        startupViewController = StartUpViewController()
        
        if let tabBarViewController = tabBarViewController {
            tabBarViewController.willMoveToParentViewController(nil)
            addChildViewController(startupViewController!)
            
            
            view.addSubview(startupViewController!.view)
            startupViewController!.view.frame = view.frame
            
            startupViewController!.view.alpha = 0
            startupViewController!.view.transform = CGAffineTransformMakeScale(1.5, 1.5)
            
            self.transitionFromViewController(tabBarViewController, toViewController: startupViewController!, duration: 0.6, options: .CurveEaseInOut, animations: { 
                
                self.startupViewController!.view.alpha = 1
                self.startupViewController!.view.transform = CGAffineTransformMakeScale(1, 1)
                
                }, completion: { finished in
                    self.tabBarViewController?.removeFromParentViewController()
                    self.startupViewController?.didMoveToParentViewController(self)
                    self.currentViewController = self.startupViewController
                    self.tabBarViewController = nil
            })
            return
        }
        
        view.addSubview(startupViewController!.view)
        
        addChildViewController(startupViewController!)
        startupViewController?.didMoveToParentViewController(self)
    
        currentViewController = startupViewController
        tabBarViewController = nil
    }
    
    func showTabBarViewController() {
        if let currentViewController = currentViewController,
            let tabBarViewController = tabBarViewController where currentViewController == tabBarViewController{
            return
        }
        
        tabBarViewController = UITabBarController()

        
        tabBarViewController?.viewControllers = [ UINavigationController(rootViewController:ScheduleViewController()),UINavigationController(rootViewController: MyScheduleViewController()), UINavigationController(rootViewController:BugReportViewController())]
        
        self.tabBarViewController?.tabBar.items![0].title = "Розклад"
        self.tabBarViewController?.tabBar.items![0].image = UIImage(named: "schedule_icon")!.imageWithRenderingMode(.AlwaysOriginal)
        self.tabBarViewController?.tabBar.items![0].selectedImage = UIImage(named: "schedule_selected")!.imageWithRenderingMode(.AlwaysOriginal)
        
        self.tabBarViewController?.tabBar.items![1].title = "Мій Розклад"
        self.tabBarViewController?.tabBar.items![1].image = UIImage(named: "person")!.imageWithRenderingMode(.AlwaysOriginal)
        self.tabBarViewController?.tabBar.items![1].selectedImage = UIImage(named: "person_selected")!.imageWithRenderingMode(.AlwaysOriginal)
        
        self.tabBarViewController?.tabBar.items![2].title = "Bug Report"
        self.tabBarViewController?.tabBar.items![2].image = UIImage(named: "bug")!.imageWithRenderingMode(.AlwaysOriginal)
        self.tabBarViewController?.tabBar.items![2].selectedImage = UIImage(named: "bug_selected")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarViewController!.selectedIndex = 1
        tabBarViewController!.selectedViewController = tabBarViewController!.viewControllers![1]
        
        tabBarViewController!.view.backgroundColor = .whiteColor()
        
        if let startupViewController = startupViewController {
           
            let whiteCover = UIView()
            whiteCover.backgroundColor = .whiteColor()
            whiteCover.alpha = 0
            UIApplication.sharedApplication().keyWindow?.addSubview(whiteCover)
            whiteCover.frame = self.view.bounds
            UIView.animateWithDuration(0.1, animations: {
                whiteCover.alpha = 1
                }, completion: { finished in

                    startupViewController.dismissViewControllerAnimated(false, completion: nil)
                    
                    startupViewController.willMoveToParentViewController(nil)
                    
                    self.addChildViewController(self.tabBarViewController!)
                    
                    
                    self.view.addSubview(self.tabBarViewController!.view)
                    self.tabBarViewController!.view.frame = self.view.frame
                    
                    
                    
                    self.startupViewController?.removeFromParentViewController()
                    self.tabBarViewController?.didMoveToParentViewController(self)
                    self.currentViewController = self.tabBarViewController
                    
                    
                    UIView.animateWithDuration(1, animations: {
                        whiteCover.alpha = 0
                        }, completion: { (finished) in
                            whiteCover.removeFromSuperview()
                    })
            })
            
            
            
            

            return
        }
        
        view.addSubview(tabBarViewController!.view)
        
        addChildViewController(tabBarViewController!)
        tabBarViewController?.didMoveToParentViewController(self)
        
        
        currentViewController = tabBarViewController
        startupViewController = nil

    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(observer!)
    }
    
    
}

//extension UITabBarController {
//    public override func preferredStatusBarStyle() -> UIStatusBarStyle {
//        if let rootViewController = self.viewControllers!.first {
//            return rootViewController.preferredStatusBarStyle()
//        }
//        return self.preferredStatusBarStyle()
//    }
//}
