//
//  SelectViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit
import RealmSwift

class SelectViewController: UIViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var bottomButton: BigBlueButton!
    @IBOutlet weak var backButton: UIButton!
    
    var data: [Object]
    var dataSource: (Object -> String)!
    var onFinish: ((vc:SelectViewController, selectedObject:Object) -> Void)!
    var onBack: (SelectViewController -> Void)?
    var text = ""
    var buttonText = ""
    
    private var pickerDataSource: SelectViewControllerPickerDataSource!
    
    init(data: [Object]) {
        
        self.data = data
        
        
        super.init(nibName: "SelectViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
 
    func reload() {
        pickerView?.reloadAllComponents()
    }
    
    func animateDisappearing(onFinish: (SelectViewController) -> Void) {
        UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.topLabel.frame.origin.y = -self.topLabel.frame.height
            self.bottomButton.frame.origin.y = self.view.frame.height
            self.pickerView.alpha = 0
            
            }) { (completed) in
                onFinish(self)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if onBack == nil {
            backButton.hidden = true
        }
        
        pickerDataSource = SelectViewControllerPickerDataSource(data: data, dataSource: dataSource)
        
        pickerView.dataSource = pickerDataSource
        pickerView.delegate = pickerDataSource
        
        bottomButton.setTitle(buttonText, forState: .Normal)
        topLabel.text = text
        
        bottomButton.addTarget(self, action: #selector(bottomButtonPressed), forControlEvents: .TouchUpInside)
        
        backButton.addTarget(self, action: #selector(backButtonPressed), forControlEvents: .TouchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed() {
        onBack?(self)
    }

    func bottomButtonPressed() {
        onFinish(vc: self, selectedObject: data[pickerView.selectedRowInComponent(0)])
    }
    
}

class SelectViewControllerPickerDataSource: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var data: [Object]
    var dataSource: Object -> String
    
    init(data: [Object], dataSource: Object -> String) {
        self.data = data
        self.dataSource = dataSource
        super.init()
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource(data[row])
    }
}

