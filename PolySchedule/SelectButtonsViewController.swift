//
//  SelectButtonsViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class SelectButtonsViewController: UIViewController {

    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomTextLabel: UILabel!
    
    @IBOutlet weak var firstButton: BigBlueButton!
    @IBOutlet weak var secondButton: BigBlueButton!
    @IBOutlet weak var thirdButton: BigBlueButton!
    @IBOutlet weak var forthButton: BigBlueButton!
    
    var data: [String]
    var onSelect: ((vc:SelectButtonsViewController, selectedString:String) -> Void)!
    var onBack: (SelectButtonsViewController -> Void)?
    var text = ""
    var infoText = ""
    
    init(data: [String]) {
        self.data = data
        super.init(nibName: "SelectButtonsViewController", bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        switch data.count {
        case 0:
            fatalError("No data")
        case 1:
            secondButton.hidden = true
            thirdButton.hidden = true
            forthButton.hidden = true
            firstButton.setTitle(data[0], forState: .Normal)
        case 2:
            thirdButton.hidden = true
            forthButton.hidden = true
            secondButton.setTitle(data[0], forState: .Normal)
            firstButton.setTitle(data[1], forState: .Normal)
        case 3:
            forthButton.hidden = true
            thirdButton.setTitle(data[0], forState: .Normal)
            secondButton.setTitle(data[1], forState: .Normal)
            firstButton.setTitle(data[2], forState: .Normal)
        default:
            forthButton.setTitle(data[0], forState: .Normal)
            thirdButton.setTitle(data[1], forState: .Normal)
            secondButton.setTitle(data[2], forState: .Normal)
            firstButton.setTitle(data[3], forState: .Normal)
        }
        
        topLabel.text = text
        bottomTextLabel.text = infoText
        
        if onBack == nil {
            backButton.hidden = true
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func didTapButton(sender: BigBlueButton) {
        onSelect(vc: self, selectedString: data[sender.tag - 1])
    }
    
    @IBAction func didTapBackButton(sender: UIButton) {
        onBack?(self)
    }
    

}
