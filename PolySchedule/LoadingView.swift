//
//  LoadingView.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    var topServerImageView: UIImageView = UIImageView()
    var firstServerItemView = UIImageView()
    var secondServerItemView = UIImageView()
    var thirdServerItemView = UIImageView()
    
    var iPhoneImageView = UIImageView()
    var firstPhoneItem = UIView()
    var secondPhoneItem = UIView()
    var thirdPhoneItem = UIView()
    
    var blue_server_item = UIImage(named: "data_item_blue")
    var green_server_item = UIImage(named: "data_item_green")
    
    var greenColor = UIColor(red:0.40, green:0.78, blue:0.81, alpha:1.00)
    var greyColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
    
    
    var isAnimating = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    private func initialize() {
        topServerImageView.image = UIImage(named: "top_data_item")
        firstServerItemView.image = green_server_item
        secondServerItemView.image = green_server_item
        thirdServerItemView.image = green_server_item
        
        iPhoneImageView.image = UIImage(named: "iphone")
        
        firstPhoneItem.backgroundColor = greyColor
        secondPhoneItem.backgroundColor = greyColor
        thirdPhoneItem.backgroundColor = greyColor
        
        addSubview(topServerImageView)
        addSubview(firstServerItemView)
        addSubview(secondServerItemView)
        addSubview(thirdServerItemView)
        
        addSubview(iPhoneImageView)
        
        addSubview(firstPhoneItem)
        addSubview(secondPhoneItem)
        addSubview(thirdPhoneItem)
    }
    
    func startAnimation(item: Int) {
//        if isAnimating {
//            return
//        }
//        isAnimating = true
        
        switch item {
        case 1:
            UIView.transitionWithView(firstServerItemView, duration: 0.3, options: .TransitionCrossDissolve, animations: {
                self.firstServerItemView.image = self.blue_server_item
            }) { (finished) in
                UIView.animateWithDuration(0.3, animations: {
                    self.thirdPhoneItem.backgroundColor = self.greenColor
                })
            }
        case 2:
            UIView.transitionWithView(secondServerItemView, duration: 0.3, options: .TransitionCrossDissolve, animations: {
                self.secondServerItemView.image = self.blue_server_item
            }) { (finished) in
                UIView.animateWithDuration(0.3, animations: {
                    self.secondPhoneItem.backgroundColor = self.greenColor
                })
            }
        case 3:
            UIView.transitionWithView(thirdServerItemView, duration: 0.3, options: .TransitionCrossDissolve, animations: {
                self.thirdServerItemView.image = self.blue_server_item
            }) { (finished) in
                UIView.animateWithDuration(0.3, animations: {
                    self.firstPhoneItem.backgroundColor = self.greenColor
                })
            }
        default:
           // fatalError("Unknown item")
            break
        }
        
        
    }
    
    func stopAnimation() {
        layer.removeAllAnimations()
        
        topServerImageView.image = UIImage(named: "top_data_item")
        firstServerItemView.image = green_server_item
        secondServerItemView.image = green_server_item
        thirdServerItemView.image = green_server_item
        
        iPhoneImageView.image = UIImage(named: "iphone")
        
        firstPhoneItem.backgroundColor = greyColor
        secondPhoneItem.backgroundColor = greyColor
        thirdPhoneItem.backgroundColor = greyColor
    }
    
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //top 30x8
        //item 30x12
        //iphone 32x57
        
        //phone_item 22x11
        
        topServerImageView.frame = CGRect(x: 1, y: 0, width: 30, height: 8)
        firstServerItemView.frame = CGRect(x: 1, y: 7, width: 30, height: 12)
        secondServerItemView.frame = CGRect(x: 1, y: 18, width: 30, height: 12)
        thirdServerItemView.frame = CGRect(x: 1, y: 29, width: 30, height: 12)
        
        iPhoneImageView.frame = CGRect(x: 0, y: 90, width: 32, height: 57)
        
        firstPhoneItem.frame = CGRect(x: 5, y: 100, width: 22, height: 11)
        secondPhoneItem.frame = CGRect(x: 5, y: 112.5, width: 22, height: 11)
        thirdPhoneItem.frame = CGRect(x: 5, y: 124.5, width: 22, height: 11)
        
    }

}
