//
//  FontHelper.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import UIKit


struct FontHelper {
    static func bigButtonFont() -> UIFont {
        return UIFont.systemFontOfSize(22, weight: UIFontWeightLight)
    }
    
    static func bigHeaderFont() -> UIFont {
        return UIFont.systemFontOfSize(30, weight: UIFontWeightLight)
    }
    
    static func lightFontOfSize(size: CGFloat) -> UIFont {
        return UIFont.systemFontOfSize(size, weight: UIFontWeightLight)
    }
}