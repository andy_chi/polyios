//
//  DataStorage.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

class DataStorage {
    private(set) var realm: Realm
    
    private init() {
        // If DB isn't accessible throw an exception
        realm = try! Realm()
    }

    static let sharedStorage = DataStorage()
    
    func saveContentsOf<S: SequenceType where S.Generator.Element : Object>(objs: S, update: Bool = true) {
        try! realm.write {
            realm.add(objs, update: true)
        }
    }
    
    func save(obj: Object, update: Bool = true) {
        self.saveContentsOf([obj])
    }
    
    func fetchInstitutes(predicate: String = "NONE", args: [AnyObject] = []) -> Results<Institute> {
        let result = self.realm.objects(Institute)
        
        guard predicate != "NONE" else {
            return result
        }
        
        return result.filter(predicate, args)
    }
    
    func fetchInstitute(withIndex index: Int) -> Institute? {
        return fetchInstitutes("index == %@", args: [index]).first
    }
    
    func fetchGroup(withIndex index: Int) -> Group? {
        return realm.objects(Group).filter("index == %@", index).first
    }
    
    func fetchSchedule(forGroup group: Group) -> Schedule? {
        return realm.objects(Schedule).filter("id == %@", group.index).first
    }
    
    
}