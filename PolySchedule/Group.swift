//
//  Group.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

class Group: Object {
    dynamic var index: Int = 0
    dynamic var name: String = ""
    
    var institute: Institute? {
        return linkingObjects(Institute.self, forProperty: "groups").first
    }
    
    override class func primaryKey() -> String? {
        return "index"
    }
}