//
//  StartUpViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/11/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit
import RealmSwift

class StartUpViewController: UIViewController {

    var transitioningDelegateObject = ViewControllerTransitionDelegate(animator: FadeScaleTransitionAnimator())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = UIColor(red:0.24, green:0.36, blue:0.47, alpha:1.00)
        
        setupViews()
        
        Backend.getInstitutes { (error, request, response, insts) in
            
            guard error == nil else {
                if error!.domain == NSURLErrorDomain && error!.code == -1009 {
                    return self.changeStatusLabelText("Немає підключення до Інтернету\nСпробуйте пізніше", completion: nil)
                } else {
                    return self.changeStatusLabelText("Невідома помилка", completion: nil)
                }
            }
            
            if let response_ = response where response_.statusCode > 400 {
                return self.changeStatusLabelText("Помилка сервера\nСпробуйте пізніше", completion: nil)
            }
            
            DataStorage.sharedStorage.saveContentsOf(insts)
            
            self.changeStatusLabelText("Готово!", completion: { (finished) in
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(Double(NSEC_PER_SEC) * 0.7)), dispatch_get_main_queue()) {
                    self.showSelectInstituteViewController(DataStorage.sharedStorage.fetchInstitutes().filter{ _ in true })
                }
            })
            
        }

    }

    
    func changeStatusLabelText(text: String, animamted: Bool = true, completion: (Bool -> Void)?) {
        let label = self.view.subviews.filter({ $0.tag == 123 }).first
        guard let statusLabel = label as? UILabel else {
            return
        }
        
        UIView.transitionWithView(statusLabel, duration: 0.4, options: .TransitionFlipFromBottom, animations: { 
            statusLabel.text = text
            }, completion: completion)
    }
    
    
    func setupViews() {
        let helloLabel = UILabel(frame: CGRectZero)
        helloLabel.font = FontHelper.bigHeaderFont()
        
        helloLabel.textColor = .whiteColor()
        helloLabel.textAlignment = .Center
        helloLabel.text = "Привіт!"
        
        view.addSubview(helloLabel)
        helloLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-25-[helloLabel]-25-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["helloLabel": helloLabel]))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-200-[helloLabel(60)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["helloLabel": helloLabel]))
        
        let statusLabel = UILabel(frame: CGRectZero)
        statusLabel.font = FontHelper.lightFontOfSize(17)
        statusLabel.textAlignment = .Center
        statusLabel.tag = 123
        statusLabel.textColor = .whiteColor()
        statusLabel.numberOfLines = 0
        statusLabel.text = "Зачекайте\n Завантаження Даних ..."
        
        view.addSubview(statusLabel)
        
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-25-[statusLabel]-25-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["statusLabel": statusLabel]))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-280-[statusLabel]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["statusLabel": statusLabel]))
    }
    

    func showSelectInstituteViewController(insts: [Institute]) {
        let selectVC = SelectViewController(data: insts)
        selectVC.text = "Виберіть ваш інститут"
        selectVC.buttonText = "Далі"
        
        selectVC.onBack = nil
        selectVC.onFinish = { (vc, obj) in
          //  vc.animateDisappearing({ (vc) in
                let groupSelectVC = SelectViewController(data: Array((obj as! Institute).groups))
                groupSelectVC.buttonText = "Далі"
                groupSelectVC.text = "Виберіть вашу групу"
                
                groupSelectVC.dataSource = { obj in
                    return (obj as! Group).name
                }
                
                groupSelectVC.onFinish = { (vc:SelectViewController, obj: Object) -> Void in
                    
                    
                    let selectSubgroup = SelectButtonsViewController(data: ["Перша","Друга","Немає"])
                    
                    selectSubgroup.onBack = { [weak selectSubgroup] _ in
                        selectSubgroup?.dismissViewControllerAnimated(true, completion: nil)
                    }
                    
                    selectSubgroup.onSelect = { [weak selectSubgroup] _, selection in
                        
                        var selectionIndex = 3
                        
                        switch selection {
                        case "Перша":
                            selectionIndex = 1
                        case "Друга":
                            selectionIndex = 2
                        case "Немає":
                            selectionIndex = 3
                        default:
                            break
                        }
                        
                        
                        let downloadingVC = DownloadingViewController(text: "Зачекайте\nРозклад завантажується", group: obj as! Group, subgroup: selectionIndex)
                        
                        downloadingVC.transitioningDelegate = self.transitioningDelegateObject
                        
                        selectSubgroup?.presentViewController(downloadingVC, animated: true, completion: nil)
                        
                    }
                    
                    selectSubgroup.text = "Виберіть вашу підгрупу"
                    selectSubgroup.infoText = "Якщо ваша група не розділена на 2 підгрупи - виберіть \"Немає\""
                    
                    
                    //selectSubgroup.modalPresentationStyle = .Custom
                    selectSubgroup.transitioningDelegate = self.transitioningDelegateObject
                    selectSubgroup.modalPresentationStyle = UIModalPresentationStyle.FullScreen
                    vc.presentViewController(selectSubgroup, animated: true, completion: nil)
                    
//
                }
                
                groupSelectVC.onBack = { [weak groupSelectVC] _ in
                    groupSelectVC?.dismissViewControllerAnimated(true, completion: nil)
                }
                
                groupSelectVC.modalPresentationStyle = UIModalPresentationStyle.FullScreen
                groupSelectVC.transitioningDelegate = self.transitioningDelegateObject
                //TODO: completion
                vc.presentViewController(groupSelectVC, animated: true, completion: nil)
                
                
           // })
        }
        
        selectVC.dataSource = { obj in
            return (obj as! Institute).name
        }
        
        selectVC.modalPresentationStyle = UIModalPresentationStyle.FullScreen
        selectVC.transitioningDelegate = self.transitioningDelegateObject
        
        self.presentViewController(selectVC, animated: true, completion: nil)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

 
}

