//
//  DownloadingViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class DownloadingViewController: UIViewController {
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var textLabel: UILabel!
    var text: String
    
    var group: Group
    var subgroup: Int
    // subgroup: 1 - first, 2 - second, 3 - none
    init(text: String,group: Group, subgroup: Int) {
        self.text = text
        self.group = group
        self.subgroup = subgroup
        super.init(nibName: "DownloadingViewController", bundle: nil)
    }
    
    var currentProgress = 0
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.text = text
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        if !userDefaults.boolForKey("startup_completed") {
            getSchedule()
        }
    }
    
    func getSchedule() {
        Backend.getSchedule(group, callback: { (error, request, response, schedule) in
            dispatch_async(dispatch_get_main_queue()) {
                guard error == nil else {
                    if error!.domain == NSURLErrorDomain && error!.code == -1009 {
                        self.loadingView.stopAnimation()
                        return self.textLabel.text = "Немає підключення до Інтернету\nСпробуйте пізніше"
                    } else {
                        self.loadingView.stopAnimation()
                        return self.textLabel.text = "Невідома помилка"
                    }
                }
                
                if let response_ = response where response_.statusCode > 400 {
                    self.loadingView.stopAnimation()
                    return self.textLabel.text = "Помилка сервера\nСпробуйте пізніше"
                }
                
                self.saveUserData()
                DataStorage.sharedStorage.save(schedule!)
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC)), dispatch_get_main_queue(), {
                    UIView.animateWithDuration(0.6, animations: {
                        self.loadingView.alpha = 0
                    })
                    
                    UIView.transitionWithView(self.textLabel, duration: 0.4, options: .TransitionCrossDissolve, animations: {
                        self.textLabel.text = "Готово!"
                        }, completion: { (finished) in
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC)), dispatch_get_main_queue(), {
                                let tutotial = TutorialViewController()
                                
                                tutotial.transitioningDelegate = self.transitioningDelegate
                                
                                self.presentViewController(tutotial, animated: true, completion: nil)
                            })
                            
                    })
                })
                
                
                
            }
        }) { (bytesRead, totalBytesRead, totalBytesExpectedToRead) in
            dispatch_async(dispatch_get_main_queue()) {
                let progress = Int((totalBytesRead / totalBytesExpectedToRead) * 3)
                
                if progress == self.currentProgress { return }
                
                if self.currentProgress == 0 && progress == 2 {
                    self.loadingView.startAnimation(1)
                    self.loadingView.startAnimation(2)
                    self.currentProgress = progress
                    return
                }
                
                if self.currentProgress == 0 || self.currentProgress == 1 && progress == 3 {
                    self.loadingView.startAnimation(1)
                    self.loadingView.startAnimation(2)
                    self.loadingView.startAnimation(3)
                    self.currentProgress = progress
                    return
                }
                
                self.currentProgress = progress
                self.loadingView.startAnimation(progress)
                
            }
        }

    }
    
    func saveUserData() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(true, forKey: "startup_completed")
        userDefaults.setInteger(subgroup, forKey: "subgroup_key")
        userDefaults.setInteger(group.index, forKey: "group_index")
       
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
