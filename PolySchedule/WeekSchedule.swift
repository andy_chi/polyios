//
//  WeekSchedule.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

class WeekSchedule: Object {
    dynamic var id: String = "" // identifier field form json
    
    let days = List<DaySchedule>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}