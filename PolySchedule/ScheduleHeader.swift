//
//  ScheduleHeader.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class ScheduleHeader: UIView { // 32

    let dateLabel = UILabel()
    let dayLabel = UILabel()
    
    var separator = CALayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red:0.98, green:0.99, blue:1.00, alpha:1.00)
        
        dateLabel.font = UIFont.systemFontOfSize(15, weight: UIFontWeightRegular)
        dateLabel.textColor = UIColor(red:0.49, green:0.58, blue:0.69, alpha:1.00)
        dateLabel.textAlignment = .Right
        
        dayLabel.font = UIFont.systemFontOfSize(15, weight: UIFontWeightRegular)
        dayLabel.textColor = UIColor(red:0.49, green:0.58, blue:0.69, alpha:1.00)
        dayLabel.textAlignment = .Left
        
        self.addSubview(dateLabel)
        self.addSubview(dayLabel)
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dayLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[dayLabel]-0-|", options: NSLayoutFormatOptions(rawValue:0), metrics: nil, views: ["dayLabel" : dayLabel]))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[dateLabel]-0-|", options: NSLayoutFormatOptions(rawValue:0), metrics: nil, views: ["dateLabel" : dateLabel]))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[dayLabel]-0-|", options: NSLayoutFormatOptions(rawValue:0), metrics: nil, views: ["dayLabel" : dayLabel]))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[dateLabel]-8-|", options: NSLayoutFormatOptions(rawValue:0), metrics: nil, views: ["dateLabel" : dateLabel]))
        
        layer.addSublayer(separator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        separator.frame = CGRect(x: 0, y: frame.size.height - 1.5, width: frame.size.width, height: 1.5)
        separator.backgroundColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.00).CGColor
        
    }

}
