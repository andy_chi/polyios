//
//  ViewControllerTransitionDelegate.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/15/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerTransitionDelegate:NSObject, UIViewControllerTransitioningDelegate {
    private(set) var animator: ANDViewControllerAnimatedTransitioning
    
    init(animator: ANDViewControllerAnimatedTransitioning) {
        self.animator = animator
    }
    
    private override init() {
        fatalError("Use init(:ANDViewControllerAnimatedTransitioning) instead")
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        animator.presenting = true
        return animator
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        animator.presenting = false
        return animator
    }
    
    
}


