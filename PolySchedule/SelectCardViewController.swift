//
//  SelectCardViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit
import RealmSwift

protocol SelectCardViewControllerDelegate: NSObjectProtocol {
    func didTapSelectButton(vc: SelectCardViewController)
}

class SelectCardViewController: CardContainerViewController {
    
    enum SelectItem:Int, Hashable {
        case Institute = 0
        case Group = 1
        case Subgroup = 2
        case Semester = 3
    }
    
    let cellHeight = 72
    let buttonHeight = 55
    
    var pickerView = UIPickerView()
    
    weak var delegate: SelectCardViewControllerDelegate?
    
    var tableView = UITableView()
    var button = BigBlueButton(type: .System)
    
    let selectItems: [SelectItem]
    
    var currentInstitute: Institute?
    var currentGroup: Group?
    var currentSubgroup: Int? // 1-3 (first) 2 (second)
    var currentSemester: Int? // 1-4
    
    var institutes: Results<Institute>!
    
    var selectedItem: SelectItem!
    
    init(selectItems: [SelectItem]) {
        self.selectItems = selectItems
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tableSize = selectItems.count * cellHeight
        
        tableView.allowsSelection = false
        tableView.separatorStyle = .None
        
        heightConstraint.constant = CGFloat(tableSize) + CGFloat(buttonHeight) + headerHeightConstraint.constant
        
        contentView.addSubview(tableView)
        button.layer.cornerRadius = 0
        button.titleLabel?.font = UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)
        
        button.addTarget(self, action: #selector(didTapSelectButton), forControlEvents: .TouchUpInside)
        
        containerView.addSubview(button)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        button.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[tableView]-0-[button(\(buttonHeight))]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["tableView": tableView, "button": button]))
        
        containerView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[tableView]-0-|", options:  NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["tableView": tableView]))
        containerView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[button]-0-|", options:  NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button": button]))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.scrollEnabled = false
        
        tableView.registerNib(UINib(nibName: "SelectTableViewCell",bundle: nil), forCellReuseIdentifier: "selectCell")
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        institutes = DataStorage.sharedStorage.fetchInstitutes()
        
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnView)))
    }
    
    override var inputView: UIView? {
        return pickerView
    }
    
    override func viewWillLayoutSubviews() {
        self.resignFirstResponder()
    }
    
    func didTapSelectButton() {
        delegate?.didTapSelectButton(self)
    }
    
    func didTapOnView() {
        if self.isFirstResponder() {
            self.resignFirstResponder()
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
}


extension SelectCardViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("selectCell", forIndexPath: indexPath) as! SelectTableViewCell
        
        let selectItem = selectItems[indexPath.row]
        
        switch selectItem {
        case .Group:
            cell.topLabel.text = "група"
            cell.bottomLabel.text = currentGroup!.name
        case .Institute:
            cell.bottomLabel.text = currentInstitute!.name
            cell.topLabel.text = "інститут"
        case .Semester:
            if currentSemester! == 1 {
                cell.bottomLabel.text = "Осінь ч.1"
            }
            if currentSemester! == 2 {
                cell.bottomLabel.text = "Осінь ч.2"
            }
            if currentSemester! == 3 {
                cell.bottomLabel.text = "Весна ч.1"
            }
            if currentSemester! == 4 {
                cell.bottomLabel.text = "Весна ч.2"
            }
            
            cell.topLabel.text = "семестер"
        case .Subgroup:
            if currentSubgroup! == 2 {
                cell.bottomLabel.text = "2-га"
            } else if currentSubgroup! == 1 {
                cell.bottomLabel.text = "1-ша"
            } else {
                cell.bottomLabel.text = "None"
            }
            cell.topLabel.text = "підгрупа"
        }
        
        cell.indexPath = indexPath
        
        cell.selectHandler = { [weak self] indexPath in
            
            
            
            self?.selectedItem = self?.selectItems[indexPath.row]
            
            self?.pickerView.reloadAllComponents()
            
            switch self!.selectedItem! {
            case .Institute:
                self?.pickerView.selectRow(self!.institutes.indexOf(NSPredicate(format: "id == %@", self!.currentInstitute!.id))!, inComponent: 0, animated: false)
            case .Group:
                self?.pickerView.selectRow(self!.currentInstitute!.groups.indexOf({ (group) -> Bool in
                    return group.index == self!.currentGroup!.index
                })!, inComponent: 0, animated: false)
            case .Semester:
                self?.pickerView.selectRow(self!.currentSemester! - 1, inComponent: 0, animated: false)
            case .Subgroup:
                self?.pickerView.selectRow(self!.currentSubgroup! - 1, inComponent: 0, animated: false)
            }
            
            if self?.isFirstResponder() ?? true {
                self?.resignFirstResponder()
            } else {
                self?.becomeFirstResponder()
            }
            
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(cellHeight)
    }
}

extension SelectCardViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedItem == nil {
            return 0
        }
        
        switch selectedItem! {
        case .Institute:
            return institutes.count
        case .Group:
            return currentInstitute!.groups.count
        case .Semester:
            return 4
        case .Subgroup:
            return 3
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch selectedItem! {
        case .Institute:
            return institutes[row].name
        case .Group:
            return currentInstitute?.groups[row].name
        case .Subgroup:
            return row == 0 ? "1-ша" : (row == 1) ? "2-га" : "None"
        case .Semester:
            switch row {
            case 0:
                return "Осінь ч.1"
            case 1:
                return "Осінь ч.2"
            case 2:
                return "Весна ч.1"
            case 3:
                return "Весна ч.2"
            default:
                break
            }
        }
        
        return nil
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch selectedItem! {
        case .Institute:
            currentInstitute = institutes[row]
            currentGroup = currentInstitute!.groups[0]
        case .Group:
            currentGroup = currentInstitute!.groups[row]
        case .Subgroup:
            currentSubgroup = row + 1
        case .Semester:
            currentSemester = row + 1
        }
        
        tableView.reloadData()
    }
}
