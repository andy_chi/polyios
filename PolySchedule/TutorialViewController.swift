//
//  TutorialViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

let STARTUP_COMPLETED_NOTIFICATION = "com.andy.ch.poly.STARTUP_COMPLETED_NOTIFICATION"

class TutorialViewController: UIViewController {

    @IBOutlet weak var confirmButton: BigBlueButton!
    
    init() {
        super.init(nibName: "TutorialViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapConfirmButton(sender: BigBlueButton) {
        sender.enabled = false
        NSNotificationCenter.defaultCenter().postNotificationName(STARTUP_COMPLETED_NOTIFICATION, object: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
