//
//  InstitutesParser.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import SwiftyJSON

class InstitutesParser: ResponseParser {
    func parse(data: NSData, callback: (NSError?, [Institute]?) -> Void) {
    
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)) {
            var error: NSError?
            let  json = JSON(data: data, options: NSJSONReadingOptions.AllowFragments , error: &error)
            
            guard error == nil && json.array != nil else {
                dispatch_async(dispatch_get_main_queue()) {
                    callback(error, [])
                }
                return
            }
            
            let insts = json.array!
            
            var result = [Institute]()
            
            for jsonInst in insts {
                
                guard let id = jsonInst["_id"].string,
                    let indexString = jsonInst["value"].string,
                    let name = jsonInst["name"].string,
                    let groups = jsonInst["groups"].array,
                    let index = Int(indexString)
                else {
                    continue
                }
                
                let inst = Institute()
                inst.id = id
                inst.index = index
                inst.name = name
                
                inst.groups.appendContentsOf(groups.reduce([Group](), combine: { (accum, json) -> [Group] in
                    do {
                        return  accum + [(try self.parseGroup(json))]
                    } catch {
                        return accum
                    }
                }))
                
                result.append(inst)
                
                //print(insts)
                
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                callback(nil, result)
            }
        }
        
    }
    
    func parseGroup(groupJSON: JSON) throws -> Group {
        let group = Group()
        
        guard let indexString = groupJSON["value"].string, let name = groupJSON["name"].string, let index = Int(indexString) else {
            throw NSError(domain: "Missing field", code: 404, userInfo: nil)
        }
        
        group.index = index
        group.name = name
        
        return group
    }
}