//
//  BigBlueButton.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class BigBlueButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    func initialize() {
        setBackgroundImage(UIImage(), forState: .Highlighted)
        titleLabel?.font = FontHelper.bigButtonFont()
        setTitleColor(.whiteColor(), forState: .Normal)
        layer.cornerRadius = 2.5
        
        backgroundColor = UIColor(red:0.23, green:0.72, blue:0.98, alpha:1.00)
    }

}
