//
//  ANDViewControllerAnimatedTransitioning.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/15/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

protocol ANDViewControllerAnimatedTransitioning : UIViewControllerAnimatedTransitioning {
    var presenting: Bool { get set }
}