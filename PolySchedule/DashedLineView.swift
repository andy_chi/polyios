//
//  DashedLineView.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class DashedLineView: UIView {

    private var lineLayer = CAShapeLayer()
    var vertical = true
    var lineColor = UIColor.redColor() {
        didSet {
            setNeedsLayout()
        }
    }
    
    init(frame: CGRect, vertical: Bool) {
        super.init(frame: frame)
        
        self.vertical = vertical
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    func initialize() {
        layer.addSublayer(lineLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        UIColor.clearColor().setStroke()
        let path = UIBezierPath()
        
        let endPoint = vertical ? CGPoint(x: 0 ,y: frame.size.height) : CGPoint(x: frame.size.width, y: 0)
        
        path.moveToPoint(CGPoint(x: 0,y: 0))
        path.addLineToPoint(endPoint)
        
        path.stroke()
        
        lineLayer.frame = bounds
        lineLayer.strokeStart = 0
        
        lineLayer.lineWidth = vertical ? frame.size.width : frame.size.height
        lineLayer.lineJoin = kCALineJoinBevel
        lineLayer.lineDashPattern = [2,2]
        lineLayer.fillColor = UIColor.clearColor().CGColor
        lineLayer.strokeColor = lineColor.CGColor
        lineLayer.lineDashPhase = 3
        lineLayer.path = path.CGPath
        
    }

}
