//
//  DaySchedule.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

enum ScheduleDay: Int {
    case Monday = 0
    case Tuesday = 1
    case Wednesday = 2
    case Thursday = 3
    case Friday = 4
    case Saturday = 5
    case Sunday = 6
}

class DaySchedule: Object {
    dynamic var id: String = "" //Composed number + weekSchedule.id
    
    /// corresponds day of week. Monday 0 - Sunday 6
    var dayName: ScheduleDay {
        get {
            return ScheduleDay(rawValue: numberRaw)!
        } set {
            numberRaw = dayName.rawValue
        }
    }
    dynamic var numberRaw: Int = 0
    
    var weekSchedule: WeekSchedule? {
       return linkingObjects(WeekSchedule.self, forProperty: "days").first
    }
    
    let lessons = List<Lesson>()
    
    var numberOfEmptyLessons : Int {
        get {
            return lessons.last!.number - lessons.count
        }
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}