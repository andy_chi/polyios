//
//  LessonDetailViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit
import MapKit

class LessonDetailViewController: UIViewController {

    @IBOutlet weak var mapSnapshotImageView: UIImageView!
    @IBOutlet weak var lessonTypeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lessonTypeImageView: UIImageView!
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    
    let date: NSDate
    let lessonPart: LessonPart
    
    init(lessonPart: LessonPart, date: NSDate) {
    
        self.lessonPart = lessonPart
        self.date = date
        
        super.init(nibName: "LessonDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let weekday = NSDateFormatter()
        weekday.dateFormat = "EEEE"
        weekday.locale = NSLocale.currentLocale()
        
        lessonTypeLabel.text = lessonPart.lessonType!.description
        locationLabel.text = lessonPart.location
        teacherLabel.text = lessonPart.teacher
        titleLabel.text = lessonPart.title
        
        switch lessonPart.lessonType! {
        case .Lab:
            lessonTypeImageView.image = UIImage(named: "lab")
        case .Lecture:
            lessonTypeImageView.image = UIImage(named: "lecture")
        case .Practice:
            lessonTypeImageView.image = UIImage(named: "practice")
        }
        
        title = "\(lessonPart.lesson!.number) пара"
        mapSnapshotImageView.contentMode = .ScaleAspectFill
        mapSnapshotImageView.clipsToBounds = true
        
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        activityIndicator.center = mapSnapshotImageView.center
        
        
        
        guard let loc = lessonPart.mapLocation else {
            return
        }
        
        activityIndicator.startAnimating()
        
        let options = MKMapSnapshotOptions()
        options.showsBuildings = true
        options.mapType = .Hybrid
        options.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.0019, longitudeDelta: 0.0019))
        
        MKMapSnapshotter(options: options).startWithCompletionHandler { [weak self] (snapshot, error) -> Void in
            if error == nil && self != nil {
                self?.mapSnapshotImageView.image = snapshot!.image
                self?.activityIndicator.stopAnimating()
                //imageCache.setObject(snapshot!.image, forKey: self!.camping.Address)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
