//
//  MyScheduleViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/17/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

var token = dispatch_once_t()

class MyScheduleViewController: UITableViewController {

    var group: Group?
    var schedule: WeekSchedule?
    var currentWeekTop = true
    var currentSeasonPart = 4
    var subgroup = 0
    
    var tableHeader = UILabel()
    
    let noDataLabel = UILabel()

    
    var prevIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    
        tableView.separatorStyle = .None
        tableView.layoutMargins = UIEdgeInsetsZero

        let userDefaults = NSUserDefaults.standardUserDefaults()
        
         self.group = DataStorage.sharedStorage.fetchGroup(withIndex: userDefaults.integerForKey("group_index"))
        
        
        if let group = self.group {
            navigationItem.title = group.institute!.name + ", " + group.name
        }
        
        self.subgroup = NSUserDefaults.standardUserDefaults().integerForKey("subgroup_key")
        
        let navItemButton = UIBarButtonItem(title: "Змінити", style: .Plain, target: self, action: #selector(changeButtonDidTap))
        
        navigationItem.setRightBarButtonItem(navItemButton, animated: false)
        
        setNeedsStatusBarAppearanceUpdate()
        
        
        tableView.registerNib(UINib(nibName: "OneLessonTableViewCell", bundle: nil), forCellReuseIdentifier: "mainCell")
        
        tableView.registerNib(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "emptyCell")
        
        tableHeader.font = UIFont.systemFontOfSize(13, weight: UIFontWeightRegular)
        tableHeader.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.00)
        tableHeader.frame = CGRect(x: 0, y: 0, width: 100, height: 33)
        
        tableHeader.textAlignment = .Center
        
        tableView.tableHeaderView = tableHeader
        
        noDataLabel.textColor = UIColor(red:0.50, green:0.50, blue:0.50, alpha:1.00)
        noDataLabel.font = UIFont.systemFontOfSize(18, weight: UIFontWeightRegular)
        noDataLabel.text = "Ого го!\nА розкладу немає :("
        noDataLabel.numberOfLines = 3
        noDataLabel.textAlignment = .Center
        view.addSubview(noDataLabel)
        
        noDataLabel.hidden = true
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        dispatch_once(&token) {
            self.fetchData()
        }
    }
    
    
    func fetchData() {
        
        navigationItem.title = group!.institute!.name + ", " + group!.name
        let fullSchedule = DataStorage.sharedStorage.fetchSchedule(forGroup: group!)
        
        switch currentSeasonPart {
        case 1:
            schedule = fullSchedule!.autumn_part1
        case 2:
            schedule = fullSchedule!.autumn_part2
        case 3:
            schedule = fullSchedule!.spring_part1
        case 4:
            schedule = fullSchedule!.spring_part2
        default:
            break
        }
        updateTableHeader()
        
        if schedule?.days.count == 0 {
            noDataLabel.hidden = false
            noDataLabel.frame = view.bounds
        } else {
            noDataLabel.hidden = true
        }
        
        tableView.reloadData()
    }
    
    func updateTableHeader() {
        tableHeader.text = ""
        
        if currentSeasonPart == 1 {
            tableHeader.text = "Осінь ч.1"
        }
        if currentSeasonPart == 2 {
            tableHeader.text = "Осінь ч.2"
        }
        if currentSeasonPart == 3 {
            tableHeader.text = "Весна ч.1"
        }
        if currentSeasonPart == 4 {
            tableHeader.text = "Весна ч.2"
        }
        
        tableHeader.text! +=  ", поточний тиждень"
        
        if subgroup == 1 {
           tableHeader.text! += ", 1-ша підгрупа"
        } else if subgroup == 2 {
           tableHeader.text! += ", 2-га підгрупа"
        }
        
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  schedule?.days.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedule!.days[section].numberOfEmptyLessons + schedule!.days[section].lessons.count
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ScheduleHeader()
    
        
        let day = schedule!.days[section]
        
        switch day.dayName {
        case .Monday:
            header.dayLabel.text = "Понеділок"
        case .Tuesday:
            header.dayLabel.text = "Вівторок"
        case .Wednesday:
            header.dayLabel.text = "Середа"
        case .Thursday:
            header.dayLabel.text = "Четвер"
        case .Friday:
            header.dayLabel.text = "П'ятниця"
        case .Saturday:
            header.dayLabel.text = "Субота"
        case .Sunday:
            header.dayLabel.text = "Неділя"
        }
        
        header.dateLabel.text = ""
        
        return header
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let day = schedule!.days[indexPath.section]
        
        
        
       let lessonExists = day.lessons.indexOf({ $0.number == indexPath.row + 1 })
        
        if lessonExists == nil  {
           return emptyCell(indexPath)
        }
        
//        
        let lesson = day.lessons[lessonExists!]
//      
        let lessonPart = getLessonPart(lesson)
        
        
        if lessonPart == nil {
            return emptyCell(indexPath)
        }
        

        let cell = tableView.dequeueReusableCellWithIdentifier("mainCell", forIndexPath: indexPath) as! OneLessonTableViewCell
        
        
        
        cell.lessonPart = lessonPart
        

        return cell
    }
    
    func emptyCell(indexPath: NSIndexPath) -> EmptyTableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("emptyCell", forIndexPath: indexPath) as! EmptyTableViewCell
        
        return cell
    }
    
    func getLessonPart(lesson: Lesson) -> LessonPart? {
        var lessonPart: LessonPart?
        
        x:if subgroup == 2 {
            
            if currentWeekTop {
                
                if lesson.number == 5 {
                    
                }
                
                if lesson.firstRow.count == 0 {
                    return nil
                } else if lesson.firstRow.count == 1 {
                    lessonPart = lesson.firstRow.first!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                } else if lesson.firstRow.count == 2 {
                    lessonPart = lesson.firstRow.last!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    
                    break x
                }
                
            } else {
                if lesson.secondRow.count == 0 {
                    return nil
                } else if lesson.secondRow.count == 1 {
                    lessonPart = lesson.secondRow.first!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                } else if lesson.secondRow.count == 2 {
                    lessonPart = lesson.secondRow.last!
                    
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                }
            }
            
        } else { // 1 subgroup
            
            if currentWeekTop {
                
                if lesson.firstRow.count == 0 {
                    return nil
                } else if lesson.firstRow.count == 1 {
                    lessonPart = lesson.firstRow.first!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                } else if lesson.firstRow.count == 2 {
                    lessonPart = lesson.firstRow.first!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                }
                
            } else {
                if lesson.secondRow.count == 0 {
                    return nil
                } else if lesson.secondRow.count == 1 {
                    lessonPart = lesson.secondRow.first!
                    if lessonPart!.type == .Empty {
                        return nil
                    }
                    break x
                } else if lesson.secondRow.count == 2 {
                    lessonPart = lesson.secondRow.first!
                    
                    if lessonPart!.type == .Empty {
                        return nil
                    } 
                    break x
                }
            }
            
        }
        
        
        
        return lessonPart

    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let day = schedule!.days[indexPath.section]
        
        let lessonExists = day.lessons.indexOf({ $0.number == indexPath.row + 1 })
        
        if lessonExists == nil {
            return 20
        }
        
        let lesson = day.lessons[lessonExists!]
     
        let lessonPart = getLessonPart(lesson)
        
        if lessonPart == nil {
            return 20
        }
        
        return 60
    }
 

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)
        
        if selectedCell is EmptyTableViewCell {
            return
        }
                
        let lessonDetailVC = LessonDetailViewController(lessonPart: (selectedCell as! OneLessonTableViewCell).lessonPart!, date: NSDate())
        
        self.navigationController?.pushViewController(lessonDetailVC, animated: true)
        
    }
    
    //MARK: - Actions
    
    func changeButtonDidTap() {
        let card = SelectCardViewController(selectItems: [.Institute, .Group, .Subgroup, .Semester])
        card.button.setTitle("Готово", forState: .Normal)
        card.title = "Виберіть вашу групу"
        
        card.currentInstitute = group?.institute
        card.currentGroup = group
        card.currentSemester = currentSeasonPart
        card.currentSubgroup = subgroup
        card.delegate = self
        self.tabBarController?.definesPresentationContext = true
         self.tabBarController?.modalPresentationStyle = .CurrentContext
        self.tabBarController?.presentViewController(card, animated: true, completion: nil)
    }

    
    func downloadData() {
        Backend.getSchedule(group!,callback: { (error, request, response, schedule) in
            if schedule == nil {
                print("Error occured")
                return
            }
            
            DataStorage.sharedStorage.save(schedule!)
            self.fetchData()
            self.view.userInteractionEnabled = true
            let userDefaults = NSUserDefaults.standardUserDefaults()
            
            userDefaults.setInteger(self.subgroup, forKey: "subgroup_key")
            
            userDefaults.setInteger(self.group!.index, forKey: "group_index")
            userDefaults.synchronize()
        })
    }
}

extension MyScheduleViewController : SelectCardViewControllerDelegate {
    func didTapSelectButton(vc: SelectCardViewController) {
        var downloadNeeded = false
        var reloadNeeded = false
        
        if group!.index != vc.currentGroup!.index {
            group = vc.currentGroup
            downloadNeeded = true
        }
        
        if subgroup != vc.currentSubgroup! {
            subgroup = vc.currentSubgroup!
            reloadNeeded = true
            let userDefaults = NSUserDefaults.standardUserDefaults()
            
            userDefaults.setInteger(subgroup, forKey: "subgroup_key")
        }
        
        if currentSeasonPart != vc.currentSemester! {
            currentSeasonPart = vc.currentSemester!
            reloadNeeded = true
        }
        
        vc.dismissViewControllerAnimated(true, completion: nil)
        
        
        if downloadNeeded {
            view.userInteractionEnabled = false
            downloadData()
        } else if reloadNeeded {
            fetchData()
        }
        
        
    }
}
