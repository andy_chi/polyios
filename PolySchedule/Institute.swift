//
//  Institute.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift

class Institute: Object {
    dynamic var id: String = ""
    dynamic var index: Int = 0
    dynamic var name: String = ""
    
    let groups = List<Group>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}