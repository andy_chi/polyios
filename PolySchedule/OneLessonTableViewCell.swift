//
//  OneLessonTableViewCell.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class OneLessonTableViewCell: UITableViewCell {

    @IBOutlet weak var lineView: DashedLineView!
    @IBOutlet weak var lessonNumberLabel: UILabel!
    @IBOutlet weak var lessonTypeImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    
    var separator = CALayer()
    
    var lessonPart: LessonPart? {
        didSet {
            if lessonPart == nil {
                fatalError("lessonPart is nil")
            }
            
            if lessonPart!.type == .Empty  {
                fatalError("lessonPart is empty part")
            }
            
            lessonNumberLabel.text = String(lessonPart!.lesson!.number)
            
            
            switch  lessonPart!.lessonType! {
            case .Lab:
                lessonTypeImageView.image = UIImage(named: "lab_mini")
            case .Lecture:
                lessonTypeImageView.image = UIImage(named: "lecture_mini")
            case .Practice:
                lessonTypeImageView.image = UIImage(named: "practice_mini")
            }
            
            titleLabel.text = lessonPart!.title
            locationLabel.text = lessonPart!.location
            teacherLabel.text = lessonPart!.teacher
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lineView.lineColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.00)
        lineView.vertical = true
        lineView.backgroundColor = .clearColor()
        
        
        selectedBackgroundView = UIView()
        
        selectedBackgroundView?.backgroundColor = UIColor(red:0.56, green:0.68, blue:0.78, alpha:0.3)
       // selectionStyle = .None
        layoutMargins = UIEdgeInsetsZero
        
        layer.addSublayer(separator)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        separator.frame = CGRect(x: 0, y: frame.size.height - 1, width: frame.size.width, height: 1.5)
        separator.backgroundColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.00).CGColor
        
    }

    
}
