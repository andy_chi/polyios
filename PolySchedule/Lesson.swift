//
//  Lesson.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import RealmSwift
import MapKit

/*

 -'FSM' : 'First Subgroup Middle'.
 -'SSM' : 'Second Subgroup Middle'.
 -'STD' : 'Standard' lesson cell. Contains full info about lesson.
 -'EMP' : 'Empty' lessson cell. Acts as placeholder. Does not contain any data.

*/

enum LessonPartType: String {
    case Standard = "STD"
    case Empty = "EMP"
    case SecondSubgroupMiddle = "SSM"
    case FirstSubgroupMiddle = "FSM"
}

enum LessonPartLessonType: String, CustomStringConvertible {
    case Practice = "прак."
    case Lab = "лаб."
    case Lecture = "лекція"
    
    
    var description: String {
        switch self {
        case .Practice:
            return "Практична"
        case .Lab:
            return "Лабораторна"
        case .Lecture:
            return "Лекція"
        }
    }
}

class Lesson: Object {
    dynamic var number: Int = 0
    
    let firstRow = List<LessonPart>()
    let secondRow = List<LessonPart>()
}

class LessonPart: Object {
    dynamic var title = ""
    dynamic var location = ""
    dynamic var teacher = ""
    dynamic var row = 1 // 1-2
    
  
    var type: LessonPartType {
        get {
            return LessonPartType(rawValue: typeRaw) ?? .Empty
        } set {
            typeRaw = type.rawValue
        }
    }
    
    dynamic var typeRaw = ""
    
    var lessonType: LessonPartLessonType? {
        get {
            return LessonPartLessonType(rawValue: lessonTypeRaw)
        } set {
            lessonTypeRaw = lessonType?.rawValue ?? ""
        }
    }
    
    var lesson: Lesson? {
        if let lesson = linkingObjects(Lesson.self, forProperty: "firstRow").first {
            return lesson
        } else {
            return linkingObjects(Lesson.self, forProperty: "secondRow").first
        }
    }
    
    dynamic var lessonTypeRaw = ""
}

extension LessonPart {
    var mapLocation: CLLocation? {
        if location.hasPrefix("гол") {
            return CLLocation(latitude: 49.8354929, longitude: 24.014442)
        }
        if location.hasPrefix("I ") {
            return CLLocation(latitude: 49.83535986, longitude: 24.0105933)
        }
        if location.hasPrefix("II ") {
            return CLLocation(latitude: 49.8360586, longitude: 24.0123996)
        }
        if location.hasPrefix("III ") {
            return CLLocation(latitude: 49.83647569, longitude: 24.01371807)
        }
        if location.hasPrefix("IV ") {
            return CLLocation(latitude: 49.8364221, longitude: 24.0111458)
        }
        if location.hasPrefix("V ") {
            return CLLocation(latitude: 49.83522492, longitude: 24.00873184)
        }
        if location.hasPrefix("VI ") {
            return CLLocation(latitude: 49.835323, longitude: 24.0062115)
        }
        if location.hasPrefix("VII ") {
            return CLLocation(latitude: 49.83460558, longitude: 24.00973231)
        }
        if location.hasPrefix("VIII ") {
            return CLLocation(latitude: 49.83732855, longitude: 24.0149492)
        }
        if location.hasPrefix("IX ") {
            return CLLocation(latitude: 49.83642033, longitude: 24.01434571)
        }
        if location.hasPrefix("X ") {
            return CLLocation(latitude: 49.8363875, longitude: 24.0152221)
        }
        if location.hasPrefix("XI ") {
            return CLLocation(latitude: 49.8358953, longitude: 24.0271023)
        }
    
        if location.hasPrefix("XIII ") {
            return CLLocation(latitude: 49.83565223, longitude: 24.01570022)
        }
        if location.hasPrefix("XIV ") {
            return CLLocation(latitude: 49.8354446, longitude: 24.0165411)
        }
        if location.hasPrefix("XV ") {
            return CLLocation(latitude: 49.8354446, longitude: 24.0165411)
        }
        if location.hasPrefix("XVI ") {
            return CLLocation(latitude: 49.8354446, longitude: 24.0165411)
        }
        if location.hasPrefix("XVII ") {
            return CLLocation(latitude: 49.8354446, longitude: 24.0165411)
        }
     
        if location.hasPrefix("XIX ") {
            return CLLocation(latitude: 49.83827999, longitude: 24.03353155)
        }
 
        if location.hasPrefix("XXXIII ") {
            return CLLocation(latitude: 49.84093, longitude: 24.029401)
        }
        
        return nil
    }
    
}