//
//  EmptyTableViewCell.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    @IBOutlet weak var lineView: DashedLineView!
    
    var separator = CALayer()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lineView.lineColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.00)
        lineView.vertical = true
        lineView.backgroundColor = .clearColor()
        
        selectionStyle = .None
        
        separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        layoutMargins = UIEdgeInsetsZero
    
        layer.addSublayer(separator)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        separator.frame = CGRect(x: 0, y: frame.size.height - 1.5, width: frame.size.width, height: 1.5)
        separator.backgroundColor = UIColor(red:0.91, green:0.94, blue:0.97, alpha:1.00).CGColor
    }
    
}
