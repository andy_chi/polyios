//
//  CardContainerViewController.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/18/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit



class CardContainerViewController: UIViewController, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate{

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    
    init() {
        super.init(nibName: "CardContainerViewController", bundle: nil)
        self.transitioningDelegate = self
        //self.modalTransitionStyle = .PartialCurl
        self.modalPresentationStyle = .OverFullScreen
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = title
        containerView.layer.cornerRadius = 3
        containerView.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapCloseButton(sender: UIButton) {
        dismiss()
    }
    
    func dismiss() { //TODO:
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        if toViewController == self { // presenting
            fromViewController.view.userInteractionEnabled = false
            
            transitionContext.containerView()?.addSubview(fromViewController.view)
            transitionContext.containerView()?.addSubview(toViewController.view)
            
            toViewController.view.frame = fromViewController.view.frame
            
            toViewController.view.alpha = 0
            toViewController.view.transform = CGAffineTransformMakeScale(1.5, 1.5)
            
            UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
                
                
                toViewController.view.alpha = 1
                toViewController.view.transform = CGAffineTransformIdentity
                
                }, completion: { (completed) in
                    fromViewController.view.userInteractionEnabled = true
                    transitionContext.completeTransition(completed)
            })
        } else {
            
            fromViewController.view.userInteractionEnabled = false
            
            transitionContext.containerView()?.addSubview(toViewController.view)
            transitionContext.containerView()?.addSubview(fromViewController.view)
            
            
            UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
                
                
                fromViewController.view.alpha = 0
                self.contentView.transform = CGAffineTransformMakeScale(0.9, 0.9)
                
                }, completion: { (completed) in
                    transitionContext.completeTransition(true)
                    UIApplication.sharedApplication().keyWindow!.addSubview(UIApplication.sharedApplication().keyWindow!.rootViewController!.view)
                    
            })
        }

    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    
    
}
