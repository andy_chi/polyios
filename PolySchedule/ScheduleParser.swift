//
//  ScheduleParser.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/16/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation
import SwiftyJSON

class ScheduleParser: ResponseParser {
    
    func parse(data: NSData, callback: (NSError?, Schedule?) -> Void) {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)) {
            var error: NSError?
            let  json = JSON(data: data, options: NSJSONReadingOptions.AllowFragments , error: &error)
            
            guard error == nil && json.array != nil else {
                dispatch_async(dispatch_get_main_queue()) {
                    callback(error, nil)
                }
                return
            }
            
            let scheduleJSON = json.array!
            
            let schedule = Schedule()
            
            do {
                schedule.autumn_part1 = try self.parseWeekSchedule(scheduleJSON.filter({ (json) -> Bool in
                     json["identifier"].string!.hasSuffix("semestr=0&semest_part=0")
                }).first!)
                schedule.autumn_part2 = try self.parseWeekSchedule(scheduleJSON.filter({ (json) -> Bool in
                    return json["identifier"].string!.hasSuffix("semestr=0&semest_part=1")
                }).first!)
                
                schedule.spring_part1 = try self.parseWeekSchedule(scheduleJSON.filter({ (json) -> Bool in
                    return json["identifier"].string!.hasSuffix("semestr=1&semest_part=0")
                }).first!)
                schedule.spring_part2 = try self.parseWeekSchedule(scheduleJSON.filter({ (json) -> Bool in
                    return json["identifier"].string!.hasSuffix("semestr=1&semest_part=1")
                }).first!)
            } catch {
                callback(NSError(domain: "SCHEDULE_PARSER_ERROR", code: 101, userInfo: nil), nil)
                return
            }
        
            
            callback(nil, schedule)
        }

    }
    
  
    
    
    func parseWeekSchedule(schedule: JSON) throws -> WeekSchedule {
        let weekSchedule = WeekSchedule()
        
        weekSchedule.id = schedule["identifier"].string!
        
        
        
        if let monday = schedule["schedule"]["Пн"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(monday, dayNumber: 0, weekId: weekSchedule.id))
        }
        
        if let tuesday = schedule["schedule"]["Вт"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(tuesday, dayNumber: 1, weekId: weekSchedule.id))
        }
        
        if let wednesday = schedule["schedule"]["Ср"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(wednesday, dayNumber: 2, weekId: weekSchedule.id))
        }
        
        if let thursday = schedule["schedule"]["Чт"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(thursday, dayNumber: 3, weekId: weekSchedule.id))
        }
        
        if let friday = schedule["schedule"]["Пт"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(friday, dayNumber: 4, weekId: weekSchedule.id))
        }
        
        if let saturday = schedule["schedule"]["Сб"].dictionary {
            weekSchedule.days.append(try parseDaySchedule(saturday, dayNumber: 5, weekId: weekSchedule.id))
        }
        
            
        return weekSchedule
    }
    
    func parseDaySchedule(day: [String:JSON], dayNumber: Int, weekId: String) throws -> DaySchedule {
        let daySchedule = DaySchedule()
        daySchedule.id = "\(dayNumber)-" + weekId
        
        daySchedule.numberRaw = dayNumber
        
        let lessonsNumbers = (1...10).map(String.init)
        
        for number in lessonsNumbers {
            if let lessonJSON = day[number] {
                daySchedule.lessons.append(try parseLesson(lessonJSON, number: Int(number)!))
            }
        }
        
        return daySchedule
    }
    
    func parseLesson(lessonJSON: JSON, number: Int) throws -> Lesson {
        let lesson = Lesson()
        
        lesson.number = number
        
        guard let lessonParts = lessonJSON.array else {
            throw NSError(domain: "SCHEDULE_PARSER_ERROR", code: 101, userInfo: nil)
        }
        
      
        
        try lesson.firstRow.appendContentsOf(lessonParts.map({ try parseLessonPart($0) }).filter { $0.row == 1 })
        try lesson.secondRow.appendContentsOf(lessonParts.map({ try parseLessonPart($0) }).filter { $0.row == 2 })
        
        return lesson
    }
    
    func parseLessonPart(lessonPartJSON: JSON) throws -> LessonPart {
        let lessonPart = LessonPart()
        

        
        lessonPart.typeRaw = lessonPartJSON["type"].string!
        lessonPart.row = lessonPartJSON["row"].int!
        
        guard lessonPart.type != .Empty else {
            return lessonPart
        }
        
        
        guard let lessonTypeString = lessonPartJSON["lessonType"].string,
            let location = lessonPartJSON["location"].string,
            let title = lessonPartJSON["title"].string,
            let teacher = lessonPartJSON["teacher"].string else {
                if lessonPart.type == .FirstSubgroupMiddle {
                    return lessonPart
                }
                
                throw NSError(domain: "SCHEDULE_PARSER_ERROR", code: 101, userInfo: nil)
        }
        
        lessonPart.lessonTypeRaw = lessonTypeString
        lessonPart.location = location
        lessonPart.title = title
        lessonPart.teacher = teacher
        
        return lessonPart
    }
    
}