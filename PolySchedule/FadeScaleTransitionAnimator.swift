//
//  FadeScaleTransitionAnimator.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/15/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import UIKit

class FadeScaleTransitionAnimator:NSObject, ANDViewControllerAnimatedTransitioning {
    
    var presenting: Bool = true
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        if presenting {
            fromViewController.view.userInteractionEnabled = false
            
            transitionContext.containerView()?.addSubview(fromViewController.view)
            transitionContext.containerView()?.addSubview(toViewController.view)
            
            toViewController.view.frame = fromViewController.view.frame
            
            toViewController.view.alpha = 0
            toViewController.view.transform = CGAffineTransformMakeScale(1.5, 1.5)
            
            UIView.animateWithDuration(transitionDuration(transitionContext), animations: { 
                
                
                toViewController.view.alpha = 1
                toViewController.view.transform = CGAffineTransformIdentity
                
                }, completion: { (completed) in
                    fromViewController.view.userInteractionEnabled = true
                    transitionContext.completeTransition(completed)
            })
        } else {
            
            fromViewController.view.userInteractionEnabled = false
            
            transitionContext.containerView()?.addSubview(toViewController.view)
            transitionContext.containerView()?.addSubview(fromViewController.view)
            
            
            UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
                
                
                fromViewController.view.alpha = 0
                fromViewController.view.transform = CGAffineTransformMakeScale(1.5, 1.5)
                
                }, completion: { (completed) in
                    fromViewController.view.alpha = 1
                    fromViewController.view.transform = CGAffineTransformMakeScale(1, 1)
                    fromViewController.view.userInteractionEnabled = true
                    transitionContext.completeTransition(completed)
            })
        }
    }
}
