//
//  ResponseParser.swift
//  PolySchedule
//
//  Created by Andy Chikalo on 4/14/16.
//  Copyright © 2016 andy.ch. All rights reserved.
//

import Foundation



protocol ResponseParser {
    associatedtype T
    func parse(data: NSData, callback: (NSError?, T?) -> Void)
}